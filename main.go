package main

import (
	"strings"
)

var input = "struct { int a ; int a ; struct { int a ; int a ; } a ; } a ; $"
var stack = "S"

func main() {
	stack := strings.Split(stack, "")
	input := strings.Split(input, " ")
	input, stack = ruleS(input, stack)
	println("Parsing complete!")
}

// S -> Z
func ruleS(input []string, stack []string) ([]string, []string) {
	checkStack(stack, "S")

	stack = removeItemFromStack(stack)
	stack = append(stack, "$", "Z")

	input, stack = ruleZ(input, stack)

	checkStack(stack, "$")
	stack = removeItemFromStack(stack)
	input = removeItemFromInput(input)

	return input, stack
}

// Z -> AB
func ruleZ(input []string, stack []string) ([]string, []string) {
	checkStack(stack, "Z")

	stack = removeItemFromStack(stack)
	stack = append(stack, "B", "A")

	input, stack = ruleA(input, stack)
	input, stack = ruleB(input, stack)

	return input, stack
}

// A -> struct
func ruleA(input []string, stack []string) ([]string, []string) {
	checkStack(stack, "A")
	stack = removeItemFromStack(stack)

	checkInput(input, "struct")
	input = removeItemFromInput(input)

	return input, stack
}

// B -> {C} V
func ruleB(input []string, stack []string) ([]string, []string) {
	checkStack(stack, "B")
	stack = removeItemFromStack(stack)

	checkInput(input, "{")
	input = removeItemFromInput(input)

	stack = append(stack, "C")
	input, stack = ruleC(input, stack)

	checkInput(input, "}")
	input = removeItemFromInput(input)

	stack = append(stack, "V")
	input, stack = ruleV(input, stack)

	return input, stack
}

// C -> TVE
func ruleC(input []string, stack []string) ([]string, []string) {
	checkStack(stack, "C")
	stack = removeItemFromStack(stack)
	stack = append(stack, "E", "V", "T")

	input, stack = ruleT(input, stack)
	input, stack = ruleV(input, stack)
	input, stack = ruleE(input, stack)
	return input, stack
}

// T -> int, char, float
func ruleT(input []string, stack []string) ([]string, []string) {
	checkStack(stack, "T")
	stack = removeItemFromStack(stack)

	checkInput(input, "int")
	input = removeItemFromInput(input)

	return input, stack
}

// D -> a, b, c, d, ..
func ruleD(input []string, stack []string) ([]string, []string) {
	checkStack(stack, "D")
	stack = removeItemFromStack(stack)

	checkInput(input, "a")
	input = removeItemFromInput(input)

	return input, stack
}

// D -> a, b, c, d, ..
func ruleE(input []string, stack []string) ([]string, []string) {
	checkStack(stack, "E")
	stack = removeItemFromStack(stack)

	switch getItem(input, "first") {
	case "struct":
		stack = append(stack, "Z")
		input, stack = ruleZ(input, stack)
	case "int":
		stack = append(stack, "C")
		input, stack = ruleC(input, stack)
	case "}":
		break
	default:
		panic("struct, type, or } is expected")
	}

	return input, stack
}

// V -> D;
func ruleV(input []string, stack []string) ([]string, []string) {
	checkStack(stack, "V")
	stack = removeItemFromStack(stack)

	stack = append(stack, "D")
	input, stack = ruleD(input, stack)

	checkInput(input, ";")
	input = removeItemFromInput(input)

	return input, stack
}

func checkInput(input []string, expected string) {
	if getItem(input, "first") != expected {
		errMessage := expected + " expected, got " + getItem(input, "first")
		panic(errMessage)
	}
}

func checkStack(input []string, expected string) {
	if getItem(input, "last") != expected {
		errMessage := expected + " expected, got " + getItem(input, "last")
		panic(errMessage)
	}
}

func removeItemFromInput(input []string) []string {
	ret := make([]string, 0)
	ret = append(ret, input[:0]...)
	return append(ret, input[0+1:]...)
}

func removeItemFromStack(input []string) []string {
	return input[:len(input)-1]
}

func getItem(input []string, order string) string {
	if order == "first" {
		return input[0]
	}

	return input[len(input)-1]
}
